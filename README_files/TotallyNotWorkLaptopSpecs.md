<div class="indented">
<table width="100%" class="node" summary="attributes of jb-pc">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">jb-pc</div></td></tr></thead>
 <tbody>
    <tr><td class="first">description: </td><td class="second">Notebook</td></tr>
    <tr><td class="first">product: </td><td class="second">Bravo 17 A4DDR (17FK.1)</td></tr>
    <tr><td class="first">vendor: </td><td class="second">Micro-Star International Co., Ltd.</td></tr>
    <tr><td class="first">version: </td><td class="second">REV:1.0</td></tr>
    <tr><td class="first">serial: </td><td class="second">9S717FK12022ZK5000058</td></tr>
    <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
    <tr><td class="first">capabilities: </td><td class="second"><dfn title="SMBIOS version 3.2.0">smbios-3.2.0</dfn> <dfn title="DMI version 3.2.0">dmi-3.2.0</dfn> <dfn title="Symmetric Multi-Processing">smp</dfn> <dfn title="32-bitov procesy">vsyscall32</dfn> </td></tr>
    <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of jb-pc"><tr><td class="sub-first"> boot</td><td>=</td><td>normal</td></tr><tr><td class="sub-first"> chassis</td><td>=</td><td>notebook</td></tr><tr><td class="sub-first"> family</td><td>=</td><td>Br</td></tr><tr><td class="sub-first"> sku</td><td>=</td><td>17FK.1</td></tr><tr><td class="sub-first"> uuid</td><td>=</td><td>ABE8C683-F496-41F9-A554-7EF9687F7DD8</td></tr></table></td></tr>
 </tbody></table></div>
<div class="indented">
        <div class="indented">
    <table width="100%" class="node" summary="attributes of core">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">core</div></td></tr></thead>
 <tbody>
       <tr><td class="first">description: </td><td class="second">Motherboard</td></tr>
       <tr><td class="first">product: </td><td class="second">MS-17FK</td></tr>
       <tr><td class="first">vendor: </td><td class="second">Micro-Star International Co., Ltd.</td></tr>
       <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
       <tr><td class="first">version: </td><td class="second">REV:1.0</td></tr>
       <tr><td class="first">serial: </td><td class="second">BSS-0123456789</td></tr>
       <tr><td class="first">slot: </td><td class="second">Default string</td></tr>
 </tbody>    </table></div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of firmware">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">firmware</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">BIOS</td></tr>
          <tr><td class="first">vendor: </td><td class="second">American Megatrends Inc.</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
          <tr><td class="first">version: </td><td class="second">E17FKAMS.113</td></tr>
          <tr><td class="first">date: </td><td class="second">05/18/2020</td></tr>
          <tr><td class="first">size: </td><td class="second">64KiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">16MiB</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="PCI bus">pci</dfn> <dfn title="BIOS EEPROM can be upgraded">upgrade</dfn> <dfn title="BIOS shadowing">shadowing</dfn> <dfn title="Booting from CD-ROM/DVD">cdboot</dfn> <dfn title="Selectable boot path">bootselect</dfn> <dfn title="Enhanced Disk Drive extensions">edd</dfn> <dfn title="5.25&quot; 1.2MB floppy">int13floppy1200</dfn> <dfn title="3.5&quot; 720KB floppy">int13floppy720</dfn> <dfn title="3.5&quot; 2.88MB floppy">int13floppy2880</dfn> <dfn title="Print Screen key">int5printscreen</dfn> <dfn title="i8042 keyboard controller">int9keyboard</dfn> <dfn title="INT14 serial line control">int14serial</dfn> <dfn title="INT17 printer control">int17printer</dfn> <dfn title="ACPI">acpi</dfn> <dfn title="USB legacy emulation">usb</dfn> <dfn title="BIOS boot specification">biosbootspecification</dfn> <dfn title="UEFI specification is supported">uefi</dfn> </td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of memory">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">memory</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">System Memory</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">a</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">System board or motherboard</td></tr>
          <tr><td class="first">size: </td><td class="second">40GiB</td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of bank:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">bank:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">SODIMM DDR4 Synchronous Unbuffered (Unregistered) 3200 MHz (0,3 ns)</td></tr>
             <tr><td class="first">product: </td><td class="second">KHX3200C20S4/32GX</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Kingston</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
             <tr><td class="first">serial: </td><td class="second">B4ACF060</td></tr>
             <tr><td class="first">slot: </td><td class="second">DIMM 0</td></tr>
             <tr><td class="first">size: </td><td class="second">32GiB</td></tr>
             <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">3200MHz (0.3ns)</td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of bank:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">bank:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">SODIMM DDR4 Synchronous Unbuffered (Unregistered) 3200 MHz (0,3 ns)</td></tr>
             <tr><td class="first">product: </td><td class="second">M471A1K43DB1-CWE</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Samsung</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
             <tr><td class="first">serial: </td><td class="second">365B12E7</td></tr>
             <tr><td class="first">slot: </td><td class="second">DIMM 0</td></tr>
             <tr><td class="first">size: </td><td class="second">8GiB</td></tr>
             <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">3200MHz (0.3ns)</td></tr>
 </tbody>          </table></div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">L1 cache</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">c</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">L1 - Cache</td></tr>
          <tr><td class="first">size: </td><td class="second">512KiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">512KiB</td></tr>
          <tr><td class="first">clock: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Pipeline burst">pipeline-burst</dfn> <dfn title="Internal">internal</dfn> <dfn title="Write-back">write-back</dfn> <dfn title="Unified cache">unified</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cache:0"><tr><td class="sub-first"> level</td><td>=</td><td>1</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">L2 cache</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">d</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">L2 - Cache</td></tr>
          <tr><td class="first">size: </td><td class="second">4MiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">4MiB</td></tr>
          <tr><td class="first">clock: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Pipeline burst">pipeline-burst</dfn> <dfn title="Internal">internal</dfn> <dfn title="Write-back">write-back</dfn> <dfn title="Unified cache">unified</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cache:1"><tr><td class="sub-first"> level</td><td>=</td><td>2</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:2</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">L3 cache</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">e</div></td></tr>
          <tr><td class="first">slot: </td><td class="second">L3 - Cache</td></tr>
          <tr><td class="first">size: </td><td class="second">8MiB</td></tr>
          <tr><td class="first">capacity: </td><td class="second">8MiB</td></tr>
          <tr><td class="first">clock: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Pipeline burst">pipeline-burst</dfn> <dfn title="Internal">internal</dfn> <dfn title="Write-back">write-back</dfn> <dfn title="Unified cache">unified</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cache:2"><tr><td class="sub-first"> level</td><td>=</td><td>3</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cpu">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cpu</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">CPU</td></tr>
          <tr><td class="first">product: </td><td class="second">AMD Ryzen 7 4800H with Radeon Graphics</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">f</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">cpu@0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">AMD Ryzen 7 4800H with Radeon Graphics</td></tr>
          <tr><td class="first">serial: </td><td class="second">Unknown</td></tr>
          <tr><td class="first">slot: </td><td class="second">FP6</td></tr>
          <tr><td class="first">size: </td><td class="second">1397MHz</td></tr>
          <tr><td class="first">capacity: </td><td class="second">2900MHz</td></tr>
          <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">100MHz</td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="64bits extensions (x86-64)">lm</dfn> <dfn title="mathematical co-processor">fpu</dfn> <dfn title="FPU exceptions reporting">fpu_exception</dfn> <dfn title="">wp</dfn> <dfn title="virtual mode extensions">vme</dfn> <dfn title="debugging extensions">de</dfn> <dfn title="page size extensions">pse</dfn> <dfn title="time stamp counter">tsc</dfn> <dfn title="model-specific registers">msr</dfn> <dfn title="4GB+ memory addressing (Physical Address Extension)">pae</dfn> <dfn title="machine check exceptions">mce</dfn> <dfn title="compare and exchange 8-byte">cx8</dfn> <dfn title="on-chip advanced programmable interrupt controller (APIC)">apic</dfn> <dfn title="fast system calls">sep</dfn> <dfn title="memory type range registers">mtrr</dfn> <dfn title="page global enable">pge</dfn> <dfn title="machine check architecture">mca</dfn> <dfn title="conditional move instruction">cmov</dfn> <dfn title="page attribute table">pat</dfn> <dfn title="36-bit page size extensions">pse36</dfn> <dfn title="">clflush</dfn> <dfn title="multimedia extensions (MMX)">mmx</dfn> <dfn title="fast floating point save/restore">fxsr</dfn> <dfn title="streaming SIMD extensions (SSE)">sse</dfn> <dfn title="streaming SIMD extensions (SSE2)">sse2</dfn> <dfn title="HyperThreading">ht</dfn> <dfn title="fast system calls">syscall</dfn> <dfn title="no-execute bit (NX)">nx</dfn> <dfn title="multimedia extensions (MMXExt)">mmxext</dfn> <dfn title="">fxsr_opt</dfn> <dfn title="">pdpe1gb</dfn> <dfn title="">rdtscp</dfn> <dfn title="64bits extensions (x86-64)">x86-64</dfn> <dfn title="">constant_tsc</dfn> <dfn title="">rep_good</dfn> <dfn title="">nopl</dfn> <dfn title="">nonstop_tsc</dfn> <dfn title="">cpuid</dfn> <dfn title="">extd_apicid</dfn> <dfn title="">aperfmperf</dfn> <dfn title="">rapl</dfn> <dfn title="">pni</dfn> <dfn title="">pclmulqdq</dfn> <dfn title="">monitor</dfn> <dfn title="">ssse3</dfn> <dfn title="">fma</dfn> <dfn title="">cx16</dfn> <dfn title="">sse4_1</dfn> <dfn title="">sse4_2</dfn> <dfn title="">movbe</dfn> <dfn title="">popcnt</dfn> <dfn title="">aes</dfn> <dfn title="">xsave</dfn> <dfn title="">avx</dfn> <dfn title="">f16c</dfn> <dfn title="">rdrand</dfn> <dfn title="">lahf_lm</dfn> <dfn title="">cmp_legacy</dfn> <dfn title="">svm</dfn> <dfn title="">extapic</dfn> <dfn title="">cr8_legacy</dfn> <dfn title="">abm</dfn> <dfn title="">sse4a</dfn> <dfn title="">misalignsse</dfn> <dfn title="">3dnowprefetch</dfn> <dfn title="">osvw</dfn> <dfn title="">ibs</dfn> <dfn title="">skinit</dfn> <dfn title="">wdt</dfn> <dfn title="">tce</dfn> <dfn title="">topoext</dfn> <dfn title="">perfctr_core</dfn> <dfn title="">perfctr_nb</dfn> <dfn title="">bpext</dfn> <dfn title="">perfctr_llc</dfn> <dfn title="">mwaitx</dfn> <dfn title="">cpb</dfn> <dfn title="">cat_l3</dfn> <dfn title="">cdp_l3</dfn> <dfn title="">hw_pstate</dfn> <dfn title="">ssbd</dfn> <dfn title="">mba</dfn> <dfn title="">ibrs</dfn> <dfn title="">ibpb</dfn> <dfn title="">stibp</dfn> <dfn title="">vmmcall</dfn> <dfn title="">fsgsbase</dfn> <dfn title="">bmi1</dfn> <dfn title="">avx2</dfn> <dfn title="">smep</dfn> <dfn title="">bmi2</dfn> <dfn title="">cqm</dfn> <dfn title="">rdt_a</dfn> <dfn title="">rdseed</dfn> <dfn title="">adx</dfn> <dfn title="">smap</dfn> <dfn title="">clflushopt</dfn> <dfn title="">clwb</dfn> <dfn title="">sha_ni</dfn> <dfn title="">xsaveopt</dfn> <dfn title="">xsavec</dfn> <dfn title="">xgetbv1</dfn> <dfn title="">xsaves</dfn> <dfn title="">cqm_llc</dfn> <dfn title="">cqm_occup_llc</dfn> <dfn title="">cqm_mbm_total</dfn> <dfn title="">cqm_mbm_local</dfn> <dfn title="">clzero</dfn> <dfn title="">irperf</dfn> <dfn title="">xsaveerptr</dfn> <dfn title="">rdpru</dfn> <dfn title="">wbnoinvd</dfn> <dfn title="">cppc</dfn> <dfn title="">arat</dfn> <dfn title="">npt</dfn> <dfn title="">lbrv</dfn> <dfn title="">svm_lock</dfn> <dfn title="">nrip_save</dfn> <dfn title="">tsc_scale</dfn> <dfn title="">vmcb_clean</dfn> <dfn title="">flushbyasid</dfn> <dfn title="">decodeassists</dfn> <dfn title="">pausefilter</dfn> <dfn title="">pfthreshold</dfn> <dfn title="">avic</dfn> <dfn title="">v_vmsave_vmload</dfn> <dfn title="">vgif</dfn> <dfn title="">v_spec_ctrl</dfn> <dfn title="">umip</dfn> <dfn title="">rdpid</dfn> <dfn title="">overflow_recov</dfn> <dfn title="">succor</dfn> <dfn title="">smca</dfn> <dfn title="">sme</dfn> <dfn title="">sev</dfn> <dfn title="">sev_es</dfn> <dfn title="CPU Frequency scaling">cpufreq</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cpu"><tr><td class="sub-first"> cores</td><td>=</td><td>8</td></tr><tr><td class="sub-first"> enabledcores</td><td>=</td><td>8</td></tr><tr><td class="sub-first"> threads</td><td>=</td><td>16</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Root Complex</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">100</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:00.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node-unclaimed" summary="attributes of generic">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">generic</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">IOMMU</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir IOMMU</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:00.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of generic"><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir PCIe GPP Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">1.1</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:01.1</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:0"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:0"><tr><td class="sub-first"> irq</td><td>:</td><td>26</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>1000(size=4096)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe500000-fe6fffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d0000000(size=270532608)</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of pci">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
                <tr><td class="first">product: </td><td class="second">Navi 10 XL Upstream Port of PCI Express Switch</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:01:00.0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">c1</td></tr>
                <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci"><tr><td class="sub-first"> irq</td><td>:</td><td>33</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe600000-fe603fff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe500000-fe5fffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d0000000(size=270532608)</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of pci">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
                   <tr><td class="first">product: </td><td class="second">Navi 10 XL Downstream Port of PCI Express Switch</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:02:00.0</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">00</td></tr>
                   <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                   <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
                   <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci"><tr><td class="sub-first"> irq</td><td>:</td><td>34</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe500000-fe5fffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d0000000(size=270532608)</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of display">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">display</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Display controller</td></tr>
                      <tr><td class="first">product: </td><td class="second">Navi 14 [Radeon RX 5500/5500M / Pro 5500M]</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:03:00.0</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">c1</td></tr>
                      <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                      <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="extension ROM">rom</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of display"><tr><td class="sub-first"> driver</td><td>=</td><td>amdgpu</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                      <tr><td class="first">resources:</td><td class="second"><table summary="resources of display"><tr><td class="sub-first"> irq</td><td>:</td><td>79</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>d0000000-dfffffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>e0000000-e01fffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe500000-fe57ffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe580000-fe59ffff</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of multimedia">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Audio device</td></tr>
                      <tr><td class="first">product: </td><td class="second">Navi 10 HDMI Audio</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">0.1</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:03:00.1</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">00</td></tr>
                      <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                      <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of multimedia"><tr><td class="sub-first"> driver</td><td>=</td><td>snd_hda_intel</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                      <tr><td class="first">resources:</td><td class="second"><table summary="resources of multimedia"><tr><td class="sub-first"> irq</td><td>:</td><td>102</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe5a0000-fe5a3fff</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
                </div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir PCIe GPP Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">2.1</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:02.1</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:1"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:1"><tr><td class="sub-first"> irq</td><td>:</td><td>27</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fea00000-feafffff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node-disabled" summary="attributes of network">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">network</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Wireless interface</td></tr>
                <tr><td class="first">product: </td><td class="second">Wi-Fi 6 AX200</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Intel Corporation</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:04:00.0</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">wlp4s0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">1a</td></tr>
                <tr><td class="first">serial: </td><td class="second">*hidden*</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="">ethernet</dfn> <dfn title="Physical interface">physical</dfn> <dfn title="Wireless-LAN">wireless</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of network"><tr><td class="sub-first"> broadcast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> driver</td><td>=</td><td>iwlwifi</td></tr><tr><td class="sub-first"> driverversion</td><td>=</td><td>5.15.0-48-generic</td></tr><tr><td class="sub-first"> firmware</td><td>=</td><td>66.f1c864e0.0 cc-a0-66.ucode</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr><tr><td class="sub-first"> link</td><td>=</td><td>no</td></tr><tr><td class="sub-first"> multicast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> wireless</td><td>=</td><td>IEEE 802.11</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of network"><tr><td class="sub-first"> irq</td><td>:</td><td>82</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fea00000-fea03fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:2</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir PCIe GPP Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">2.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:02.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:2"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:2"><tr><td class="sub-first"> irq</td><td>:</td><td>28</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f000(size=4096)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe900000-fe9fffff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of network">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">network</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Ethernet interface</td></tr>
                <tr><td class="first">product: </td><td class="second">RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Realtek Semiconductor Co., Ltd.</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:05:00.0</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">enp5s0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">15</td></tr>
                <tr><td class="first">serial: </td><td class="second">*hidden*</td></tr>
                <tr><td class="first">size: </td><td class="second">100Mbit/s</td></tr>
                <tr><td class="first">capacity: </td><td class="second">1Gbit/s</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="">ethernet</dfn> <dfn title="Physical interface">physical</dfn> <dfn title="twisted pair">tp</dfn> <dfn title="Media Independent Interface">mii</dfn> <dfn title="10Mbit/s">10bt</dfn> <dfn title="10Mbit/s (full duplex)">10bt-fd</dfn> <dfn title="100Mbit/s">100bt</dfn> <dfn title="100Mbit/s (full duplex)">100bt-fd</dfn> <dfn title="1Gbit/s (full duplex)">1000bt-fd</dfn> <dfn title="Auto-negotiation">autonegotiation</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of network"><tr><td class="sub-first"> autonegotiation</td><td>=</td><td>on</td></tr><tr><td class="sub-first"> broadcast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> driver</td><td>=</td><td>r8169</td></tr><tr><td class="sub-first"> driverversion</td><td>=</td><td>5.15.0-48-generic</td></tr><tr><td class="sub-first"> duplex</td><td>=</td><td>full</td></tr><tr><td class="sub-first"> firmware</td><td>=</td><td>rtl8168h-2_0.0.2 02/26/15</td></tr><tr><td class="sub-first"> ip</td><td>=</td><td>192.168.0.12</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr><tr><td class="sub-first"> link</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> multicast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> port</td><td>=</td><td>twisted pair</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>100Mbit/s</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of network"><tr><td class="sub-first"> irq</td><td>:</td><td>36</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f000(size=256)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe904000-fe904fff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe900000-fe903fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:3">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:3</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir PCIe GPP Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">2.3</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:02.3</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:3"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:3"><tr><td class="sub-first"> irq</td><td>:</td><td>29</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe800000-fe8fffff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of storage">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">storage</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Non-Volatile memory controller</td></tr>
                <tr><td class="first">product: </td><td class="second">BG3 NVMe SSD Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Toshiba Corporation</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:06:00.0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">01</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="">storage</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="">nvm_express</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of storage"><tr><td class="sub-first"> driver</td><td>=</td><td>nvme</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of storage"><tr><td class="sub-first"> irq</td><td>:</td><td>42</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe800000-fe803fff</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of nvme0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">nvme0</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">description: </td><td class="second">NVMe device</td></tr>
                   <tr><td class="first">product: </td><td class="second">KBG30ZMV512G TOSHIBA</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                   <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">ADXA0103</td></tr>
                   <tr><td class="first">serial: </td><td class="second">305PDB6OP4HL</td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of nvme0"><tr><td class="sub-first"> nqn</td><td>=</td><td>nqn.2017-03.jp.co.toshiba:KBG30ZMV512G TOSHIBA:305PDB6OP4HL</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>live</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of namespace">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">namespace</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">NVMe namespace</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                      <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0n1</div></td></tr>
                      <tr><td class="first">size: </td><td class="second">476GiB (512GB)</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="GUID Partition Table version 1.00">gpt-1.00</dfn> <dfn title="Partitioned disk">partitioned</dfn> <dfn title="GUID partition table">partitioned:gpt</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of namespace"><tr><td class="sub-first"> guid</td><td>=</td><td>4ad8eb88-489b-4ae0-9cef-3f5ac7be00af</td></tr><tr><td class="sub-first"> logicalsectorsize</td><td>=</td><td>512</td></tr><tr><td class="sub-first"> sectorsize</td><td>=</td><td>512</td></tr></table></td></tr>
 </tbody>                   </table></div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node-unclaimed" summary="attributes of volume:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:0</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">Windows FAT volume</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">MSDOS5.0</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">FAT32</td></tr>
                         <tr><td class="first">serial: </td><td class="second">7eb5-a715</td></tr>
                         <tr><td class="first">size: </td><td class="second">95MiB</td></tr>
                         <tr><td class="first">capacity: </td><td class="second">99MiB</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="Contains boot code">boot</dfn> <dfn title="Windows FAT">fat</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:0"><tr><td class="sub-first"> FATs</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>fat</td></tr><tr><td class="sub-first"> name</td><td>=</td><td>EFI system partition</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of volume:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:1</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">reserved partition</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Windows</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0n1p2</div></td></tr>
                         <tr><td class="first">serial: </td><td class="second">9a2da8d8-e806-4945-b2fb-f4e2920af283</td></tr>
                         <tr><td class="first">capacity: </td><td class="second">127MiB</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="No filesystem">nofs</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:1"><tr><td class="sub-first"> name</td><td>=</td><td>Microsoft reserved partition</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of volume:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:2</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">Windows NTFS volume</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Windows</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">3</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0n1p3</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/media/jboksa1/(C) Systmov oddl pro Win7</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">3.1</td></tr>
                         <tr><td class="first">serial: </td><td class="second">42574d1a-0f6b-b246-8d56-17e9c61ee6e3</td></tr>
                         <tr><td class="first">size: </td><td class="second">195GiB</td></tr>
                         <tr><td class="first">capacity: </td><td class="second">195GiB</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="Windows NTFS">ntfs</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:2"><tr><td class="sub-first"> clustersize</td><td>=</td><td>4096</td></tr><tr><td class="sub-first"> created</td><td>=</td><td>2020-07-22 09:07:57</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>ntfs</td></tr><tr><td class="sub-first"> label</td><td>=</td><td>Systémový oddíl pro Win7</td></tr><tr><td class="sub-first"> mount.fstype</td><td>=</td><td>fuseblk</td></tr><tr><td class="sub-first"> mount.options</td><td>=</td><td>rw,nosuid,nodev,relatime,user_id=0,group_id=0,default_permissions,allow_other,blksize=4096</td></tr><tr><td class="sub-first"> name</td><td>=</td><td>Basic data partition</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>mounted</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of volume:3">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:3</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">Linux swap volume</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Linux</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">4</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0n1p4</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">1</td></tr>
                         <tr><td class="first">serial: </td><td class="second">192b519d-b04b-48d2-83a9-20464fbf6a46</td></tr>
                         <tr><td class="first">size: </td><td class="second">976MiB</td></tr>
                         <tr><td class="first">capacity: </td><td class="second">976MiB</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="No filesystem">nofs</dfn> <dfn title="Linux swap">swap</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:3"><tr><td class="sub-first"> filesystem</td><td>=</td><td>swap</td></tr><tr><td class="sub-first"> pagesize</td><td>=</td><td>4095</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of volume:4">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:4</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">EXT4 volume</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Linux</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">5</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0n1p5</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">1.0</td></tr>
                         <tr><td class="first">serial: </td><td class="second">ff044fe1-9181-48bb-a28c-7e457fbe2b0e</td></tr>
                         <tr><td class="first">size: </td><td class="second">140GiB</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="">journaled</dfn> <dfn title="Extended Attributes">extended_attributes</dfn> <dfn title="4GB+ files">large_files</dfn> <dfn title="16TB+ files">huge_files</dfn> <dfn title="directories with 65000+ subdirs">dir_nlink</dfn> <dfn title="needs recovery">recover</dfn> <dfn title="64bit filesystem">64bit</dfn> <dfn title="extent-based allocation">extents</dfn> <dfn title="">ext4</dfn> <dfn title="EXT2/EXT3">ext2</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:4"><tr><td class="sub-first"> created</td><td>=</td><td>2021-11-06 12:58:44</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> lastmountpoint</td><td>=</td><td>/</td></tr><tr><td class="sub-first"> modified</td><td>=</td><td>2022-10-05 08:32:34</td></tr><tr><td class="sub-first"> mount.fstype</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> mount.options</td><td>=</td><td>rw,relatime,errors=remount-ro</td></tr><tr><td class="sub-first"> mounted</td><td>=</td><td>2022-10-05 08:32:35</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>mounted</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of volume:5">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:5</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">EXT4 volume</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Linux</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">6</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/nvme0n1p6</div></td></tr>
                         <tr><td class="first">logical name: </td><td class="second"><div class="id">/home</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">1.0</td></tr>
                         <tr><td class="first">serial: </td><td class="second">835daced-e20c-4852-a5ce-05da8c08fa2f</td></tr>
                         <tr><td class="first">size: </td><td class="second">139GiB</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="">journaled</dfn> <dfn title="Extended Attributes">extended_attributes</dfn> <dfn title="4GB+ files">large_files</dfn> <dfn title="16TB+ files">huge_files</dfn> <dfn title="directories with 65000+ subdirs">dir_nlink</dfn> <dfn title="needs recovery">recover</dfn> <dfn title="64bit filesystem">64bit</dfn> <dfn title="extent-based allocation">extents</dfn> <dfn title="">ext4</dfn> <dfn title="EXT2/EXT3">ext2</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:5"><tr><td class="sub-first"> created</td><td>=</td><td>2021-11-06 12:58:45</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> lastmountpoint</td><td>=</td><td>/home</td></tr><tr><td class="sub-first"> modified</td><td>=</td><td>2022-10-05 08:32:39</td></tr><tr><td class="sub-first"> mount.fstype</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> mount.options</td><td>=</td><td>rw,relatime</td></tr><tr><td class="sub-first"> mounted</td><td>=</td><td>2022-10-05 08:32:39</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>mounted</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:4">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:4</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir Internal PCIe GPP Bridge to Bus</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">8.1</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:08.1</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:4"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:4"><tr><td class="sub-first"> irq</td><td>:</td><td>30</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>e000(size=4096)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe100000-fe4fffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>b0000000(size=270532608)</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of display">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">display</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">VGA compatible controller</td></tr>
                <tr><td class="first">product: </td><td class="second">Renoir</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:07:00.0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">c6</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="">vga_controller</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="extension ROM">rom</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of display"><tr><td class="sub-first"> driver</td><td>=</td><td>amdgpu</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of display"><tr><td class="sub-first"> irq</td><td>:</td><td>54</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>b0000000-bfffffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>c0000000-c01fffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>e000(size=256)</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe400000-fe47ffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>c0000-dffff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of generic">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">generic</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Encryption controller</td></tr>
                <tr><td class="first">product: </td><td class="second">Family 17h (Models 10h-1fh) Platform Security Processor</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.2</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:07:00.2</div></td></tr>
                <tr><td class="first">version: </td><td class="second">00</td></tr>
                <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of generic"><tr><td class="sub-first"> driver</td><td>=</td><td>ccp</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of generic"><tr><td class="sub-first"> irq</td><td>:</td><td>82</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe300000-fe3fffff</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe4c8000-fe4c9fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
                <tr><td class="first">product: </td><td class="second">Renoir USB 3.1</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.3</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:07:00.3</div></td></tr>
                <tr><td class="first">version: </td><td class="second">00</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="">xhci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>xhci_hcd</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:0"><tr><td class="sub-first"> irq</td><td>:</td><td>37</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe200000-fe2fffff</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usbhost:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost:0</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">product: </td><td class="second">xHCI Host Controller</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Linux 5.15.0-48-generic xhci-hcd</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@1</div></td></tr>
                   <tr><td class="first">logical name: </td><td class="second"><div class="id">usb1</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">5.15</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost:0"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Mouse</td></tr>
                      <tr><td class="first">product: </td><td class="second">USB OPTICAL MOUSE</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">[Maxxter]</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@1:1</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">1.00</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>1Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Keyboard</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Cypress Semiconductor Corp.</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@1:2</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">1.00</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:2</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Video</td></tr>
                      <tr><td class="first">product: </td><td class="second">HD Webcam</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">SunplusIT Inc</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">4</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@1:4</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">3.07</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-2.01</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:2"><tr><td class="sub-first"> driver</td><td>=</td><td>uvcvideo</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>500mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
                </div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usbhost:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost:1</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">product: </td><td class="second">xHCI Host Controller</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Linux 5.15.0-48-generic xhci-hcd</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@2</div></td></tr>
                   <tr><td class="first">logical name: </td><td class="second"><div class="id">usb2</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">5.15</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-3.10</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost:1"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>10000Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
                </div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">USB controller</td></tr>
                <tr><td class="first">product: </td><td class="second">Renoir USB 3.1</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.4</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:07:00.4</div></td></tr>
                <tr><td class="first">version: </td><td class="second">00</td></tr>
                <tr><td class="first">width: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="">xhci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>xhci_hcd</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of usb:1"><tr><td class="sub-first"> irq</td><td>:</td><td>54</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe100000-fe1fffff</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usbhost:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost:0</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">product: </td><td class="second">xHCI Host Controller</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Linux 5.15.0-48-generic xhci-hcd</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@3</div></td></tr>
                   <tr><td class="first">logical name: </td><td class="second"><div class="id">usb3</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">5.15</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost:0"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">USB hub</td></tr>
                      <tr><td class="first">product: </td><td class="second">4-Port USB 2.0 Hub</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Generic</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@3:2</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">1.17</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">Audio device</td></tr>
                         <tr><td class="first">product: </td><td class="second">Ozone RecX50</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Atlasinformatica</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">3</div></td></tr>
                         <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@3:2.3</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">1.00</td></tr>
                         <tr><td class="first">serial: </td><td class="second">V1</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> <dfn title="Control device">audio-control</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">USB hub</td></tr>
                         <tr><td class="first">product: </td><td class="second">4-Port USB 2.0 Hub</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Generic</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">4</div></td></tr>
                         <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@3:2.4</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">1.17</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                      </table></div>
                      </div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Bluetooth wireless interface</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Intel Corp.</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">3</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@3:3</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">0.01</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="Bluetooth wireless radio">bluetooth</dfn> <dfn title="">usb-2.01</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>btusb</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
                </div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usbhost:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost:1</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">product: </td><td class="second">xHCI Host Controller</td></tr>
                   <tr><td class="first">vendor: </td><td class="second">Linux 5.15.0-48-generic xhci-hcd</td></tr>
                   <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                   <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4</div></td></tr>
                   <tr><td class="first">logical name: </td><td class="second"><div class="id">usb4</div></td></tr>
                   <tr><td class="first">version: </td><td class="second">5.15</td></tr>
                   <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-3.10</dfn> </td></tr>
                   <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usbhost:1"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>10000Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">Generic USB device</td></tr>
                      <tr><td class="first">product: </td><td class="second">USB 10/100/1000 LAN</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Realtek</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4:1</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">30.00</td></tr>
                      <tr><td class="first">serial: </td><td class="second">000001</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-3.00</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>r8152</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>288mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>5000Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">description: </td><td class="second">USB hub</td></tr>
                      <tr><td class="first">product: </td><td class="second">4-Port USB 3.0 Hub</td></tr>
                      <tr><td class="first">vendor: </td><td class="second">Generic</td></tr>
                      <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
                      <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4:2</div></td></tr>
                      <tr><td class="first">version: </td><td class="second">1.17</td></tr>
                      <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-3.00</dfn> </td></tr>
                      <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>5000Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of usb">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">description: </td><td class="second">USB hub</td></tr>
                         <tr><td class="first">product: </td><td class="second">4-Port USB 3.0 Hub</td></tr>
                         <tr><td class="first">vendor: </td><td class="second">Generic</td></tr>
                         <tr><td class="first">physical id: </td><td class="second"><div class="id">4</div></td></tr>
                         <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4:2.4</div></td></tr>
                         <tr><td class="first">version: </td><td class="second">1.17</td></tr>
                         <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-3.00</dfn> </td></tr>
                         <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>5000Mbit/s</td></tr></table></td></tr>
 </tbody>                      </table></div>
<div class="indented">
                                                  <div class="indented">
                         <table width="100%" class="node" summary="attributes of usb">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb</div></td></tr></thead>
 <tbody>
                            <tr><td class="first">description: </td><td class="second">Mass storage device</td></tr>
                            <tr><td class="first">product: </td><td class="second">Pioneer Blu-ray Drive</td></tr>
                            <tr><td class="first">vendor: </td><td class="second">Pioneer Corporation</td></tr>
                            <tr><td class="first">physical id: </td><td class="second"><div class="id">3</div></td></tr>
                            <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4:2.4.3</div></td></tr>
                            <tr><td class="first">version: </td><td class="second">1.00</td></tr>
                            <tr><td class="first">serial: </td><td class="second">1400011212801681</td></tr>
                            <tr><td class="first">capabilities: </td><td class="second"><dfn title="">usb-3.00</dfn> <dfn title="SFF-8020i, MMC-2 (ATAPI)">atapi</dfn> </td></tr>
                            <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of usb"><tr><td class="sub-first"> driver</td><td>=</td><td>usb-storage</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>8mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>5000Mbit/s</td></tr></table></td></tr>
 </tbody>                         </table></div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node-unclaimed" summary="attributes of multimedia:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia:0</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Multimedia controller</td></tr>
                <tr><td class="first">product: </td><td class="second">Raven/Raven2/FireFlight/Renoir Audio Processor</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.5</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:07:00.5</div></td></tr>
                <tr><td class="first">version: </td><td class="second">01</td></tr>
                <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of multimedia:0"><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of multimedia:0"><tr><td class="sub-first"> memory</td><td>:</td><td>fe480000-fe4bffff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of multimedia:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia:1</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Audio device</td></tr>
                <tr><td class="first">product: </td><td class="second">Family 17h (Models 10h-1fh) HD Audio Controller</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.6</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:07:00.6</div></td></tr>
                <tr><td class="first">version: </td><td class="second">00</td></tr>
                <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of multimedia:1"><tr><td class="sub-first"> driver</td><td>=</td><td>snd_hda_intel</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of multimedia:1"><tr><td class="sub-first"> irq</td><td>:</td><td>103</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe4c0000-fe4c7fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:5">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:5</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">Renoir Internal PCIe GPP Bridge to Bus</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">8.2</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:08.2</div></td></tr>
             <tr><td class="first">version: </td><td class="second">00</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:5"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:5"><tr><td class="sub-first"> irq</td><td>:</td><td>31</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe700000-fe7fffff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of sata:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">sata:0</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">SATA controller</td></tr>
                <tr><td class="first">product: </td><td class="second">FCH SATA Controller [AHCI mode]</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:08:00.0</div></td></tr>
                <tr><td class="first">version: </td><td class="second">81</td></tr>
                <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="">sata</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">ahci_1.0</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of sata:0"><tr><td class="sub-first"> driver</td><td>=</td><td>ahci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of sata:0"><tr><td class="sub-first"> irq</td><td>:</td><td>38</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe701000-fe7017ff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of sata:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">sata:1</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">SATA controller</td></tr>
                <tr><td class="first">product: </td><td class="second">FCH SATA Controller [AHCI mode]</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">0.1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:08:00.1</div></td></tr>
                <tr><td class="first">version: </td><td class="second">81</td></tr>
                <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
                <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="">sata</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">ahci_1.0</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of sata:1"><tr><td class="sub-first"> driver</td><td>=</td><td>ahci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
                <tr><td class="first">resources:</td><td class="second"><table summary="resources of sata:1"><tr><td class="sub-first"> irq</td><td>:</td><td>41</td></tr><tr><td class="sub-first"> memory</td><td>:</td><td>fe700000-fe7007ff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of serial">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">serial</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">SMBus</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH SMBus Controller</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">51</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of serial"><tr><td class="sub-first"> driver</td><td>=</td><td>piix4_smbus</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">resources:</td><td class="second"><table summary="resources of serial"><tr><td class="sub-first"> irq</td><td>:</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of isa">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">isa</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">ISA bridge</td></tr>
             <tr><td class="first">product: </td><td class="second">FCH LPC Bridge</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">14.3</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:14.3</div></td></tr>
             <tr><td class="first">version: </td><td class="second">51</td></tr>
             <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">clock: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="">isa</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of isa"><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir PCIe Dummy Host Bridge</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">101</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:01.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:2</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir PCIe Dummy Host Bridge</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">102</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:02.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:3">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:3</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir PCIe Dummy Host Bridge</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">103</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:08.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:4">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:4</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 0</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">104</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.0</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:5">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:5</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 1</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">105</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.1</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:6">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:6</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 2</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">106</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.2</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:7">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:7</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 3</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">107</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.3</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pci:7"><tr><td class="sub-first"> driver</td><td>=</td><td>k10temp</td></tr></table></td></tr>
          <tr><td class="first">resources:</td><td class="second"><table summary="resources of pci:7"><tr><td class="sub-first"> irq</td><td>:</td><td>0</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:8">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:8</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 4</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">108</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.4</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:9">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:9</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 5</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">109</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.5</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:10">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:10</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 6</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">10a</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.6</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:11">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:11</div></td></tr></thead>
 <tbody>
          <tr><td class="first">description: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">product: </td><td class="second">Renoir Device 24: Function 7</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">10b</div></td></tr>
          <tr><td class="first">bus info: </td><td class="second"><div class="id">pci@0000:00:18.7</div></td></tr>
          <tr><td class="first">version: </td><td class="second">00</td></tr>
          <tr><td class="first">width: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">clock: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pnp00:00">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pnp00:00</div></td></tr></thead>
 <tbody>
          <tr><td class="first">product: </td><td class="second">PnP device PNP0c01</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">0</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pnp</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pnp00:00"><tr><td class="sub-first"> driver</td><td>=</td><td>system</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pnp00:01">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pnp00:01</div></td></tr></thead>
 <tbody>
          <tr><td class="first">product: </td><td class="second">PnP device PNP0b00</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pnp</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pnp00:01"><tr><td class="sub-first"> driver</td><td>=</td><td>rtc_cmos</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pnp00:02">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pnp00:02</div></td></tr></thead>
 <tbody>
          <tr><td class="first">product: </td><td class="second">PnP device PNP0f13</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">3</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pnp</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pnp00:02"><tr><td class="sub-first"> driver</td><td>=</td><td>i8042 aux</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pnp00:03">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pnp00:03</div></td></tr></thead>
 <tbody>
          <tr><td class="first">product: </td><td class="second">PnP device MSI0007</td></tr>
          <tr><td class="first">vendor: </td><td class="second">Microstep</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">4</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pnp</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pnp00:03"><tr><td class="sub-first"> driver</td><td>=</td><td>i8042 kbd</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pnp00:04">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pnp00:04</div></td></tr></thead>
 <tbody>
          <tr><td class="first">product: </td><td class="second">PnP device PNP0c02</td></tr>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">5</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="">pnp</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of pnp00:04"><tr><td class="sub-first"> driver</td><td>=</td><td>system</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of scsi:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">scsi:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">6</div></td></tr>
          <tr><td class="first">logical name: </td><td class="second"><div class="id">scsi1</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Emulated device">emulated</dfn> </td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of disk">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">disk</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">ATA Disk</td></tr>
             <tr><td class="first">product: </td><td class="second">WDC WD5000LPLX-2</td></tr>
             <tr><td class="first">vendor: </td><td class="second">Western Digital</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0.0.0</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">scsi@1:0.0.0</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/sda</div></td></tr>
             <tr><td class="first">version: </td><td class="second">1A01</td></tr>
             <tr><td class="first">serial: </td><td class="second">WD-WXG1AA9P4JT6</td></tr>
             <tr><td class="first">size: </td><td class="second">465GiB (500GB)</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="Partitioned disk">partitioned</dfn> <dfn title="MS-DOS partition table">partitioned:dos</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of disk"><tr><td class="sub-first"> ansiversion</td><td>=</td><td>5</td></tr><tr><td class="sub-first"> logicalsectorsize</td><td>=</td><td>512</td></tr><tr><td class="sub-first"> sectorsize</td><td>=</td><td>4096</td></tr><tr><td class="sub-first"> signature</td><td>=</td><td>81bafff2</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of volume:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:0</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">Windows NTFS volume</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">scsi@1:0.0.0,1</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/sda1</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">/media/jboksa1/(D) Datov disk</div></td></tr>
                <tr><td class="first">version: </td><td class="second">3.1</td></tr>
                <tr><td class="first">serial: </td><td class="second">e4d3578d-1f5e-4549-9fa7-2777ee97ac69</td></tr>
                <tr><td class="first">size: </td><td class="second">319GiB</td></tr>
                <tr><td class="first">capacity: </td><td class="second">319GiB</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Primary partition">primary</dfn> <dfn title="Windows NTFS">ntfs</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:0"><tr><td class="sub-first"> clustersize</td><td>=</td><td>4096</td></tr><tr><td class="sub-first"> created</td><td>=</td><td>2020-08-14 15:15:11</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>ntfs</td></tr><tr><td class="sub-first"> label</td><td>=</td><td>Datový disk</td></tr><tr><td class="sub-first"> mount.fstype</td><td>=</td><td>fuseblk</td></tr><tr><td class="sub-first"> mount.options</td><td>=</td><td>rw,nosuid,nodev,relatime,user_id=0,group_id=0,default_permissions,allow_other,blksize=4096</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>mounted</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of volume:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">volume:1</div></td></tr></thead>
 <tbody>
                <tr><td class="first">description: </td><td class="second">EXT4 volume</td></tr>
                <tr><td class="first">vendor: </td><td class="second">Linux</td></tr>
                <tr><td class="first">physical id: </td><td class="second"><div class="id">2</div></td></tr>
                <tr><td class="first">bus info: </td><td class="second"><div class="id">scsi@1:0.0.0,2</div></td></tr>
                <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/sda2</div></td></tr>
                <tr><td class="first">version: </td><td class="second">1.0</td></tr>
                <tr><td class="first">serial: </td><td class="second">98e87d72-9f9f-4c29-a468-f554e669be8a</td></tr>
                <tr><td class="first">size: </td><td class="second">146GiB</td></tr>
                <tr><td class="first">capacity: </td><td class="second">146GiB</td></tr>
                <tr><td class="first">capabilities: </td><td class="second"><dfn title="Primary partition">primary</dfn> <dfn title="">journaled</dfn> <dfn title="Extended Attributes">extended_attributes</dfn> <dfn title="4GB+ files">large_files</dfn> <dfn title="16TB+ files">huge_files</dfn> <dfn title="directories with 65000+ subdirs">dir_nlink</dfn> <dfn title="extent-based allocation">extents</dfn> <dfn title="">ext4</dfn> <dfn title="EXT2/EXT3">ext2</dfn> <dfn title="initialized volume">initialized</dfn> </td></tr>
                <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of volume:1"><tr><td class="sub-first"> created</td><td>=</td><td>2022-04-13 12:04:43</td></tr><tr><td class="sub-first"> filesystem</td><td>=</td><td>ext4</td></tr><tr><td class="sub-first"> label</td><td>=</td><td>Linux zálohy</td></tr><tr><td class="sub-first"> lastmountpoint</td><td>=</td><td>/run/timeshift/50678/backup</td></tr><tr><td class="sub-first"> modified</td><td>=</td><td>2022-10-05 11:00:05</td></tr><tr><td class="sub-first"> mounted</td><td>=</td><td>2022-10-05 10:00:02</td></tr><tr><td class="sub-first"> state</td><td>=</td><td>clean</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of scsi:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">scsi:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">physical id: </td><td class="second"><div class="id">7</div></td></tr>
          <tr><td class="first">logical name: </td><td class="second"><div class="id">scsi3</div></td></tr>
          <tr><td class="first">capabilities: </td><td class="second"><dfn title="Emulated device">emulated</dfn> <dfn title="SCSI host adapter">scsi-host</dfn> </td></tr>
          <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of scsi:1"><tr><td class="sub-first"> driver</td><td>=</td><td>usb-storage</td></tr></table></td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of cdrom">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cdrom</div></td></tr></thead>
 <tbody>
             <tr><td class="first">description: </td><td class="second">DVD-RAM writer</td></tr>
             <tr><td class="first">product: </td><td class="second">BD-RW   BDR-XD07</td></tr>
             <tr><td class="first">vendor: </td><td class="second">PIONEER</td></tr>
             <tr><td class="first">physical id: </td><td class="second"><div class="id">0.0.0</div></td></tr>
             <tr><td class="first">bus info: </td><td class="second"><div class="id">scsi@3:0.0.0</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/cdrom</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/cdrw</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/dvd</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/dvdrw</div></td></tr>
             <tr><td class="first">logical name: </td><td class="second"><div class="id">/dev/sr0</div></td></tr>
             <tr><td class="first">version: </td><td class="second">1.02</td></tr>
             <tr><td class="first">capabilities: </td><td class="second"><dfn title="support is removable">removable</dfn> <dfn title="Audio CD playback">audio</dfn> <dfn title="CD-R burning">cd-r</dfn> <dfn title="CD-RW burning">cd-rw</dfn> <dfn title="DVD playback">dvd</dfn> <dfn title="DVD-R burning">dvd-r</dfn> <dfn title="DVD-RAM burning">dvd-ram</dfn> </td></tr>
             <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of cdrom"><tr><td class="sub-first"> status</td><td>=</td><td>nodisc</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
       </div>
    </div>
<div class="indented">
        <div class="indented">
    <table width="100%" class="node" summary="attributes of network">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">network</div></td></tr></thead>
 <tbody>
       <tr><td class="first">description: </td><td class="second">Ethernet interface</td></tr>
       <tr><td class="first">physical id: </td><td class="second"><div class="id">1</div></td></tr>
       <tr><td class="first">bus info: </td><td class="second"><div class="id">usb@4:1</div></td></tr>
       <tr><td class="first">logical name: </td><td class="second"><div class="id">*hidden*</div></td></tr>
       <tr><td class="first">serial: </td><td class="second">*hidden*</td></tr>
       <tr><td class="first">size: </td><td class="second">100Mbit/s</td></tr>
       <tr><td class="first">capacity: </td><td class="second">1Gbit/s</td></tr>
       <tr><td class="first">capabilities: </td><td class="second"><dfn title="">ethernet</dfn> <dfn title="Physical interface">physical</dfn> <dfn title="twisted pair">tp</dfn> <dfn title="Media Independent Interface">mii</dfn> <dfn title="10Mbit/s">10bt</dfn> <dfn title="10Mbit/s (full duplex)">10bt-fd</dfn> <dfn title="100Mbit/s">100bt</dfn> <dfn title="100Mbit/s (full duplex)">100bt-fd</dfn> <dfn title="1Gbit/s">1000bt</dfn> <dfn title="1Gbit/s (full duplex)">1000bt-fd</dfn> <dfn title="Auto-negotiation">autonegotiation</dfn> </td></tr>
       <tr><td class="first">configuration:</td><td class="second"><table summary="configuration of network"><tr><td class="sub-first"> autonegotiation</td><td>=</td><td>on</td></tr><tr><td class="sub-first"> broadcast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> driver</td><td>=</td><td>r8152</td></tr><tr><td class="sub-first"> driverversion</td><td>=</td><td>v1.12.12</td></tr><tr><td class="sub-first"> duplex</td><td>=</td><td>full</td></tr><tr><td class="sub-first"> firmware</td><td>=</td><td>rtl8153a-4 v2 02/07/20</td></tr><tr><td class="sub-first"> ip</td><td>=</td><td>192.168.215.11</td></tr><tr><td class="sub-first"> link</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> multicast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> port</td><td>=</td><td>MII</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>100Mbit/s</td></tr></table></td></tr>
 </tbody>    </table></div>
    </div>