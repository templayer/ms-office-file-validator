namespace MSOfficeFileValidator.Enums;

public enum MSOfficeDocumentType
{
    WORD,
    EXCEL,
    POWERPOINT
}