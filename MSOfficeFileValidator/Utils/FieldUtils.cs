using System.Reflection;

namespace MSOfficeFileValidator.Utils;

public static class FieldUtils
{
    public static object GetInstanceField<T>(T instance, string fieldName)
    {                
        BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
        FieldInfo field = typeof(T).GetField(fieldName, bindFlags);
        return field.GetValue(instance);
    }
}