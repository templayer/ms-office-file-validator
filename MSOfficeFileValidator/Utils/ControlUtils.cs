using Avalonia.Controls;

namespace MSOfficeFileValidator.Utils;

public static class ControlUtils
{
    public static IControl? GetRootParent(IControl? control)
    {
        if (control == null) return null;
        IControl? parent = control.Parent;
        // ReSharper disable once TailRecursiveCall The thing it wants to replace this with is unreadable for a human being
        return parent == null ? control : GetRootParent(parent);
    }
}