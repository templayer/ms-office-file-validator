using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.VisualTree;
using MSOfficeFileValidator.ViewModels;
using MSOfficeFileValidator.Views;

namespace MSOfficeFileValidator
{
    public partial class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            MainViewModel mainViewModel = new();
            TopLevel? rootTopLevel = null;
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow
                {
                    DataContext = mainViewModel
                };
                rootTopLevel = desktop.MainWindow;
            }
            else if (ApplicationLifetime is ISingleViewApplicationLifetime singleViewPlatform)
            {
                singleViewPlatform.MainView = new MainView
                {
                    DataContext = mainViewModel
                };
                
                //Getting TopLevel in SingleView - https://github.com/AvaloniaUI/Avalonia/discussions/8752
                rootTopLevel = (TopLevel?)singleViewPlatform.MainView.GetVisualRoot() ;
            }

            if (rootTopLevel == null)
                throw new NotImplementedException("Root TopLevel not found!");
            
            MainViewModel.RootTopLevel = rootTopLevel;

            base.OnFrameworkInitializationCompleted();
        }
    }
}