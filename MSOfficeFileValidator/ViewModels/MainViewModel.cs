﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Platform;
using Avalonia.Media.Imaging;
using Avalonia.Platform;
using Avalonia.Platform.Storage;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Validation;
using MessageBox.Avalonia;
using MessageBox.Avalonia.BaseWindows.Base;
using MessageBox.Avalonia.DTO;
using MessageBox.Avalonia.Enums;
using MSOfficeFileValidator.Enums;
using MSOfficeFileValidator.Storages;
using MSOfficeFileValidator.Views;
using ReactiveUI;

namespace MSOfficeFileValidator.ViewModels;

[SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
public class MainViewModel : ViewModelBase
{
    private const string LongDashSeparator = "̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶ ̶";

    public static TopLevel? RootTopLevel { get; set; }

    public string ValidateButtonLabel => "Validate";
    public string SaveOutputButtonLabel => "Save";
    public string ClearOutputButtonLabel => "Clear";
    public string AboutButtonLabel => "About";

    public string SelectFileLabel => "Select either a MS Office OOXML file, or compatible (.docx, .xlsx, .odt,...)";
    public string OutputTextBoxHint => "*This is where the output will appear once validation occurs*";
    public string PathInputHint => "Insert the file path to a Word, Excel or PowerPoint document";
    /*public string selectPathButtonLabel => "Validate";*/
        
    private string _pathInputContent = string.Empty;
    public string PathInputContent { 
        get => _pathInputContent;
        set => this.RaiseAndSetIfChanged(ref _pathInputContent, value);
    }
        
    private string _outputTextBoxContent = string.Empty;
    public string OutputTextBoxContent { 
        get => _outputTextBoxContent;
        set => this.RaiseAndSetIfChanged(ref _outputTextBoxContent, value);
    }

    public ObservableCollection<MSOfficeDocumentType> DocumentTypes { get; } = new((MSOfficeDocumentType[])Enum.GetValues(typeof(MSOfficeDocumentType)));
    public ObservableCollection<FileFormatVersions> OfficeVersions { get; } = GetAllValidOfficeVersions();

    private static ObservableCollection<FileFormatVersions> GetAllValidOfficeVersions()
    {
        ObservableCollection<FileFormatVersions> result = new();

        //We need to filter out the non-valid NONE option.
        foreach (FileFormatVersions version in (FileFormatVersions[])Enum.GetValues(typeof(FileFormatVersions)))
            if (FileFormatVersions.None != version)
                result.Add(version);

        return result;
    }
        
    private MSOfficeDocumentType _selectedDocumentType = MSOfficeDocumentType.WORD; //default value
    public MSOfficeDocumentType SelectedDocumentType
    {
        get => _selectedDocumentType;
        set => this.RaiseAndSetIfChanged(ref _selectedDocumentType, value);
    }
        
    private FileFormatVersions _selectedOfficeVersion = FileFormatVersions.Microsoft365; //default value
    public FileFormatVersions SelectedOfficeVersion
    {
        get => _selectedOfficeVersion;
        set => this.RaiseAndSetIfChanged(ref _selectedOfficeVersion, value);
    }

    public readonly Dictionary<FileFormatVersions, OpenXmlValidator> Validators = new()
    {
        //TODO do it better and dynamically. The None value must be skipped!
        { FileFormatVersions.Office2007, CreateValidator(FileFormatVersions.Office2007) },
        { FileFormatVersions.Office2010, CreateValidator(FileFormatVersions.Office2010) },
        { FileFormatVersions.Office2013, CreateValidator(FileFormatVersions.Office2013) },
        { FileFormatVersions.Office2016, CreateValidator(FileFormatVersions.Office2016) },
        { FileFormatVersions.Office2019, CreateValidator(FileFormatVersions.Office2019) },
        { FileFormatVersions.Office2021, CreateValidator(FileFormatVersions.Office2021) },
        { FileFormatVersions.Microsoft365, CreateValidator(FileFormatVersions.Microsoft365) }
    };

    private static OpenXmlValidator CreateValidator(FileFormatVersions officeVersion) =>
        new(officeVersion) { MaxNumberOfErrors = 0 }; //0 equals infinity in this case

    public readonly Dictionary<MSOfficeDocumentType, FilePickerFileType> DocumentFilePickerFileTypes = new()
    {
        {
            MSOfficeDocumentType.WORD, new FilePickerFileType(
                "MS Office Word OOXML files or compatible (.docx, .dotx, .docm, .dotm, .odt)")
            {
                Patterns = new[] { "*.docx", "*.dotx", "*.docm", "*.dotm", "*.odt" },
                MimeTypes = new[]
                {
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
                    "application/vnd.ms-word.document.macroEnabled.12",
                    "application/vnd.ms-word.template.macroEnabled.12",
                    "application/vnd.oasis.opendocument.text"
                }/*, TODO I have no f*cking clue how to deal with Apple and its proprietary sh*t
                AppleUniformTypeIdentifiers = new[]
                {

                }*/
            }
        },
        {
            MSOfficeDocumentType.EXCEL, new FilePickerFileType(
                "MS Office Excel OOXML files or compatible (.xlsx, .xltx, .xlsm, .xltm, .ods)")
            {
                Patterns = new[] { "*.xlsx", "*.xltx", "*.xlsm", "*.xltm", "*.ods" },
                MimeTypes = new[]
                {
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
                    "application/vnd.ms-excel.sheet.macroEnabled.12",
                    "application/vnd.ms-excel.template.macroEnabled.12",
                    "application/vnd.oasis.opendocument.spreadsheet"
                }
            }
        },
        {
            MSOfficeDocumentType.POWERPOINT, new FilePickerFileType(
                "MS Office PowerPoint OOXML files or compatible (.pptx, .potx, .ppsx, .pptm, .sldx, .potm, .ppsm, .sldm, .odp)")
            {
                Patterns = new[]
                    { "*.pptx", "*.potx", "*.ppsx", "*.pptm", "*.sldx", "*.potm", "*.ppsm", "*.sldm", "*.odp" },
                MimeTypes = new[]
                {
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                    "application/vnd.openxmlformats-officedocument.presentationml.template",
                    "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
                    "application/vnd.openxmlformats-officedocument.presentationml.slide",
                    "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
                    "application/vnd.ms-powerpoint.template.macroEnabled.12",
                    "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
                    "application/vnd.ms-powerpoint.slide.macroEnabled.12",
                    "application/vnd.oasis.opendocument.presentation"
                }
            }
        }
    };

    private static IStorageProvider? _storageProvider;
    public static IStorageProvider? StorageProvider {
        get
        {
            if (_storageProvider != null)
                return _storageProvider;

            IStorageProvider? rootTopLevelStorageProvider = RootTopLevel?.StorageProvider;
            if (rootTopLevelStorageProvider != null){
                _storageProvider = rootTopLevelStorageProvider;
                return _storageProvider;
            }
            
            //If mainWindow is available (for example for the Desktop variant), we use it to get a storage provider.
            // If not, then we try getting the provider from the root TopLevel instance. (Web, the designer preview,...)
            //TODO doesn't work. I have ho idea how to get a TopLevel instance in a Web, preview or Android/iOS environment.
            MainWindow? mainWindow = MainWindow.Instance;
            _storageProvider = mainWindow != null ? mainWindow.StorageProvider
                : RootTopLevel != null ? AvaloniaLocator.Current.GetService<IStorageProviderFactory>()
                    ?.CreateProvider(RootTopLevel)
                                       : null;

            if (_storageProvider == null)
                throw new InvalidOperationException("StorageProvider platform implementation is not available.");

            return _storageProvider;
        }
        set => _storageProvider = value;
    }

    public void SelectPathButtonClicked()
    {
        try
        {
            FilePickerOpenOptions options = new() { AllowMultiple = false, Title = SelectFileLabel };
            FilePickerFileType[] fileTypes = { DocumentFilePickerFileTypes[SelectedDocumentType] };
            options.FileTypeFilter = new List<FilePickerFileType>(fileTypes);
            Task<IReadOnlyList<IStorageFile>> dialog = StorageProvider!.OpenFilePickerAsync(options);

            //After a file is picked...
            // ReSharper disable once VariableHidesOuterVariable
            dialog.ContinueWith(delegate(Task<IReadOnlyList<IStorageFile>> dialog)
            {
                try
                {
                    IReadOnlyList<IStorageFile> result = dialog.Result;

                    //If the user picked a file...
                    if (result == null || result.Count == 0)
                    {
                        ResetPathToFile();
                        return;
                    }

                    //Get the path, if available...
                    IStorageFile file = result[0];
                    string? pathToFile = file.TryGetUri(out Uri? fullPath) ? fullPath.AbsolutePath : null;
                    
                    //if not available, reset path in GUI
                    if (string.IsNullOrEmpty(pathToFile?.Trim()))
                    {
                        ResetPathToFile();
                        return;
                    }
                        
                    //Without this, diacritics such as "ž" in the path are shown as %C5%
                    pathToFile = HttpUtility.UrlDecode(pathToFile);

                    //We checked beforehand
                    PathInputContent = pathToFile!;
                    Log("The file path has been set to: " + Environment.NewLine + pathToFile);
                }
                catch (Exception e)
                {
                    Log("An error has occured while trying to select file path: " + e);
                    ResetPathToFile();
                }
            });
        }
        catch (Exception e)
        {
            Log("An error has occured while trying to select file path: " + e);
            ResetPathToFile();
        }
    }

    private void ResetPathToFile() => PathInputContent = string.Empty;

    private void Log(string logMessage) => OutputTextBoxContent = string.IsNullOrEmpty(OutputTextBoxContent) 
        ? logMessage 
        : OutputTextBoxContent + Environment.NewLine 
                               + LongDashSeparator + Environment.NewLine + logMessage;
        
    public void ValidateButtonClicked() => ValidateDocument(PathInputContent);

    /** Taken from https://learn.microsoft.com/en-us/office/open-xml/how-to-validate-a-word-processing-document */
    private void ValidateDocument(string filepath)
    {
        try
        { //Todo de-hardcode isEditable?
            using TypedOpenXmlPackage document = SelectedDocumentType switch
            {
                MSOfficeDocumentType.WORD =>     WordprocessingDocument.Open(filepath, true),
                MSOfficeDocumentType.EXCEL =>       SpreadsheetDocument.Open(filepath, true),
                MSOfficeDocumentType.POWERPOINT => PresentationDocument.Open(filepath, true),
                _ => throw new NotImplementedException("SelectedDocumentType " + SelectedDocumentType 
                                                       + " is not implemented for " + nameof(ValidateDocument))
            }; 
                    
            try
            {
                OpenXmlValidator validator = Validators[SelectedOfficeVersion];
                int count = 0;
                foreach (ValidationErrorInfo error in validator.Validate(document))
                {
                    count++;
                    string result = "Error nr. " +      count + Environment.NewLine
                                    + "Description: " + error.Description + Environment.NewLine
                                    + "ErrorType: " +   error.ErrorType +   Environment.NewLine
                                    + "Node: " +        error.Node +        Environment.NewLine
                                    + "Path: " +        error.Path?.XPath + Environment.NewLine
                                    + "Part: " +        error.Part?.Uri;
                    Log(result);
                }
                Log("Total number of errors: " + count);
            }

            catch (Exception e) { Log("Validating " + filepath + " has failed: " + e); }
            finally { document.Close(); }
        }
        catch (Exception e) { Log("Opening " + filepath + " has failed: " + e); }
    }
        
    public void SavePathButtonClicked()
    {
        try
        {
            FilePickerSaveOptions options = new()
            {
                Title = "Save output as...",
                SuggestedFileName = "output.txt",
                DefaultExtension = ".txt",
                FileTypeChoices = new List<FilePickerFileType>{ new("A simple text file (.txt) with UTF-8")
                    {
                        Patterns = new[]{"*.txt"},
                        MimeTypes = new[]{"text/*"},
                        AppleUniformTypeIdentifiers = new[]{"utf8PlainText"}
                    },
                    new("A simple text file (without extension) with UTF-8")
                    {
                        Patterns = new[] { "*" },
                        MimeTypes = new[] { "text/*" },
                        AppleUniformTypeIdentifiers = new[] { "utf8PlainText" }
                    }
                },
                ShowOverwritePrompt = true
            };
            
            Task<IStorageFile?> dialog = StorageProvider!.SaveFilePickerAsync(options);
            // ReSharper disable once VariableHidesOuterVariable Intentional
            dialog.ContinueWith(delegate(Task<IStorageFile?> dialog)
            {
                try
                {
                    IStorageFile? file = dialog.Result;

                    //https://github.com/AvaloniaUI/Avalonia/blob/master/samples/ControlCatalog/Pages/DialogsPage.xaml.cs
                    if (file is not null && file.CanOpenWrite)
                    {
                        file.OpenWriteAsync().ContinueWith(delegate(Task<Stream> task)
                    {
                        try
                        {
                            using StreamWriter reader = new(task.Result);
                            reader.WriteLineAsync(OutputTextBoxContent);
                        }
                        catch (Exception e)
                        {
                            Log("An error has occured while trying to save the output: " + e);
                        }
                    });
                    }
                }
                catch (Exception e) { Log("An error has occured while trying to save the output: " + e); }
            });
        }
        catch (Exception e) { Log("An error has occured for the output save dialog: " + e); }
    }

    private void SetDefaultWindowIcon(AbstractMessageBoxParams settings)
    {
        Bitmap? icon = GetBitmapFromAssets("icon.ico");
        if (icon != null)
            settings.WindowIcon = new WindowIcon(icon);
    }
        
    public void ClearPathButtonClicked()
    {
        try
        {
            MessageBoxStandardParams dialogSettings = new()
            {
                ButtonDefinitions = ButtonEnum.OkAbort,
                ContentTitle = "Confirmation",
                ContentHeader = "Do you really want to proceed?",
                ContentMessage = "We need to make sure you didn't click on this by a mistake. 😉",
                WindowIcon = new WindowIcon("icon.ico")
            };
            SetDefaultWindowIcon(dialogSettings);
                
            IMsBoxWindow<ButtonResult>? confirmationDialog = 
                MessageBoxManager.GetMessageBoxStandardWindow( dialogSettings);
            MainWindow? mainWindow = MainWindow.Instance;
            Task<ButtonResult>? task = mainWindow != null  ?  confirmationDialog.ShowDialog(mainWindow)
                                                           :  confirmationDialog.Show();
            task.ContinueWith(delegate(Task<ButtonResult> dialog)
            {
                try
                {
                    ButtonResult result = dialog.Result;

                    if (result == ButtonResult.Ok)
                        OutputTextBoxContent = string.Empty;
                }
                catch (Exception e) { Log("Clearing the output message box has failed: " + e); }
            });
        }
        catch (Exception e) { Log("Clearing the output message box has failed: " + e); }
    }
        
    public void AboutButtonClicked()
    {
        try
        {
            MessageBoxStandardParams windowSettings = new()
            {
                ButtonDefinitions = ButtonEnum.Ok,
                ContentTitle = "About",
                ContentHeader = "Microsoft Office File Validator",
                //Icon = GetBitmapFromAssets("icon.ico"), CustomParams is broken, so we have to use a Base64 hack - nope, that didn't work either
                ShowInCenter = true,
                //TODO doesn't work for both. It works for a non-custom box, but not for a custom one. ~~Solved~~ for now via a hack for a noncustom variant
                //SizeToContent = SizeToContent.Manual,
                //Height = 500,
                //CanResize = true,
                //MinHeight = 500, //TODO remove once SizeToContent starts to work properly. :-S
                
                //TODO The first line is a hack to get a custom picture, because the MessageBoxCustomParamsWithImage glitches out. :(
                ContentMessage = //TODO This hack doesn't work either - it hangs on show. :(
                    /*"![App logo](data:image/png;base64," 
                    + ConvertBitmapToBase64(GetBitmapFromAssets("icon.png")) + ")" 
                    + Environment.NewLine +*/
                    //Todo - dehardcode. Somehow. Versions will be hard to dehardcode, I think?
                    "Version: 0.9.0 " + Storage.SystemType + "-" + RuntimeInformation.ProcessArchitecture.ToString()?.ToLower()
                    + Environment.NewLine +
                    "Git repo: https://gitlab.com/templayer/ms-office-file-validator/" + Environment.NewLine +
                    "Author: Templayer" + Environment.NewLine +
                    "Contact: Templayer@seznam.cz" + Environment.NewLine +
                    "Websites: www.Templayer.cz | www.youtube.com/Templayer" + Environment.NewLine +
                    "Building platform: .NET 6.0.9" + Environment.NewLine +
                    //The FrameworkDescription already contains ".NET"
                    "Runtime platform: " + RuntimeInformation.FrameworkDescription + Environment.NewLine +
                    "GUI: Avalonia 11.0.0-preview1" + Environment.NewLine +
                    "IDE used: JetBrains Rider 2022.2.3" + Environment.NewLine +
                    "Detected operating system: " + RuntimeInformation.OSDescription
                                           + " (" + RuntimeInformation.OSArchitecture.ToString()?.ToLower() + ")"
            };
            SetDefaultWindowIcon(windowSettings);
                
            MainWindow? mainWindow = MainWindow.Instance;
            
            if (mainWindow != null)
                 MessageBoxManager.GetMessageBoxStandardWindow(windowSettings).ShowDialog(mainWindow);
            else MessageBoxManager.GetMessageBoxStandardWindow(windowSettings).Show();
        }
        catch (Exception e) { Log("Showing the 'About' window has failed: " + e); }
    }

    /** https://stackoverflow.com/questions/10889764/how-to-convert-bitmap-to-a-base64-string */
    /*private string ConvertBitmapToBase64(Bitmap? bitmap) //TODO remove - failed hack to get a custom icon in a standard MessageBox
    {
        if (bitmap == null) return string.Empty;
        MemoryStream ms = new();
        bitmap.Save(ms);
        byte[] byteImage = ms.ToArray();
        return Convert.ToBase64String(byteImage); // Get Base64
    }*/

    private Stream? GetFileFromAssets(string fileName)
    {
        try
        {
            return AvaloniaLocator.Current?.GetService<IAssetLoader>()?.Open(
                new Uri($"avares://{Assembly.GetExecutingAssembly().GetName().Name}/Assets/{fileName}" ));
        }
        catch (Exception e) { Log("Getting the file " + fileName + " from assets has failed: " + e); }
        return null;
    }

    private Bitmap? GetBitmapFromAssets(string fileName)
    {
        try
        {
            Stream? fileStream = GetFileFromAssets(fileName);
            return fileStream == null ? null : new Bitmap(fileStream);
        }
        catch (Exception e) { Log("Getting the " + fileName + " as a bitmap from assets has failed: " + e); }
        return null;
    }
        
}