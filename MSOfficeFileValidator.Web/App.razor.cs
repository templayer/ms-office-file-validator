using Avalonia.Web.Blazor;

namespace MSOfficeFileValidator.Web;

public partial class App
{
    protected override void OnParametersSet()
    {
        base.OnParametersSet();
        
        WebAppBuilder.Configure<MSOfficeFileValidator.App>()
            .SetupWithSingleViewLifetime();
    }
}