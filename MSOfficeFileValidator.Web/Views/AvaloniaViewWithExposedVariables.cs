using Avalonia.Platform.Storage;
using Avalonia.Web.Blazor;
using MSOfficeFileValidator.Utils;
using MSOfficeFileValidator.ViewModels;

namespace MSOfficeFileValidator.Web.Views;

public class AvaloniaViewWithExposedVariables :  AvaloniaView
{
    public AvaloniaViewWithExposedVariables() : base()
    {
        MainViewModel.StorageProvider = FieldUtils.GetInstanceField(this, "_storageProvider") as IStorageProvider;
    }
}